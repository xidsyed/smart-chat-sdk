package smartsdk.server.session

data class ChatSession(
    val username: String,
    val sessionId: String
)
