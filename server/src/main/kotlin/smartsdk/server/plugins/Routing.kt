package smartsdk.server.plugins

import smartsdk.server.room.RoomController
import smartsdk.server.routes.chatSocket
import smartsdk.server.routes.getAllMessages
import io.ktor.application.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject

fun Application.configureRouting() {
    val roomController by inject<RoomController>()
    install(Routing){
        chatSocket(roomController)
        getAllMessages(roomController)
    }
}
