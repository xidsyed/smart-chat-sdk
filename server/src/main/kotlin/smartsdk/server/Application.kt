package smartsdk.server

import smartsdk.server.di.mainModule
import gitlab.smartsdk.plugins.*
import io.github.smartdousha.plugins.*
import io.ktor.application.*
import org.koin.ktor.ext.Koin
import smartsdk.server.plugins.*

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // application.conf references the main function. This annotation prevents the IDE from marking it as unused.
fun Application.module() {
    install(Koin){
        modules(mainModule)
    }
    configureSockets()
    configureRouting()
    configureSerialization()
    configureMonitoring()
    configureSecurity()
}
