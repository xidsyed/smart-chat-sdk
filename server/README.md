# Smart SDK Chat Server
### Description : 

This repository holds the standard implementation of a chat-server that is intended to be used with the client-side chat sdk. 
## Installation

To install the chat SDK Ktor server, you'll need to follow these steps:

1.  Clone the repository:
    `git clone https://gitlab.com/xidsyed/smart-chat-sdk.git`
2.  Open the project directory:    
    `cd your-repo`
3.  Build the project:    
    `./gradlew build` 
4.  Run the server:    
    `./gradlew run`

## Usage

Once you've installed the chat SDK Ktor server, you can use it by sending requests to the API endpoints. Here are some examples of how to use the server:

1.  Get all chat rooms:
```    
GET /chat/rooms    
```
2.  Create a new chat room:
```
POST /chat/rooms
Content-Type: application/json

{
    "name": "My Chat Room"
}
```
3.  Get messages in a chat room:
```
GET /chat/rooms/{roomId}/messages    
```
4.  Send a message to a chat room:    
```
POST /chat/messages
Content-Type: application/json

{
    "text": "Hello, world!"
}
```

